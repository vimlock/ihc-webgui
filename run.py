#! /usr/bin/env python

import argparse
import ihc


parser = argparse.ArgumentParser(description="Start the webserver")

parser.add_argument("port", metavar="PORT",
                    type=int, default=80,
                    help="Port number to listen to")

parser.add_argument("-d", "--debug",
                    action="store_true",
                    help="Launch the webserver with debugging enabled")

args = parser.parse_args()
app = ihc.create_app()

# If running in debug mode, do not allow external connections for
# safety reasons
listenhost = None
if not args.debug:
    listenhost = '0.0.0.0'
    
app.run(host=listenhost, debug=args.debug, port=args.port)
