IHC-webgui
==========

This is program is part of a project in Centria University of Applied Sciences
with a goal to implement system that uses existing IHC (Intelligent Home Control)
output modules to control lights and other targets.

Target of this program is to provide web interface which allows users to control
the IHC modules through a web browser. Additional goal is also to implement
REST API to the web interface.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


Dependencies
------------

- [Flask framework](http://flask.pocoo.org/)
- [Jinja2](http://jinja.pocoo.org/)
- Python 3

Usage
-----

Start the dummy commander which mimicks the functionality of the not yet
implemented IHC commander.

~~~~~~~~~~~{.sh}
$ python ihc/configurator/dummy_commander.py&
~~~~~~~~~~~

Start the flask server, you can use any port you like

~~~~~~~~~~~~{.sh}
$ run.py 80
~~~~~~~~~~~~
