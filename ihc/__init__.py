from flask import Flask

import configurator
import api
import webgui

def create_app():
    app = Flask(__name__)
    app.register_blueprint(api.module, url_prefix="/api")
    app.register_blueprint(webgui.module)

    return app
