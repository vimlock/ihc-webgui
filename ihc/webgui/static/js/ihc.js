function displayWarning(msg) {
    var tmp = '<div class="alert alert-danger alert-dismissable"> \
<button type="button" class="close" data-dismiss="alert" aria-Label="Close"> \
<span aria-hidden="true">&times;</span></button> \
<p>' + msg + '</p> \
</div>';

    console.log(msg);
    $("#warning-console").html(tmp)
}

function toggleDevice(devid, state) {
    var devid = "#dev-" + devid + "-toggle";
    var tostate = state === "enabled" ? "on" : "off";
    $(devid).data("in-progress", true);
    $(devid).bootstrapToggle(tostate);
    $(devid).removeData("in-progress");
}

function monitorDevices() {
    var url = window.location.protocol + "//" + window.location.host +
	"/api/devices/" + devIds.join() + "/status/";
    var updateInterval = 10000;

    (function loop() {
	$.ajax({
	    type: "GET",
	    url: url,
	    cache: false,
	    dataType: "json",
	    contentType: "application/json",

	    success: function(data) {
		var statuses = data.statuses;
		for (i = 0; i < statuses.length; i++) {
		    var dev = statuses[i]
		    toggleDevice(dev.id, dev.state);
		}
		
	    },

	    complete: function() {
		setTimeout(loop, updateInterval);
	    },
	    
	    timeout: updateInterval,
	});
    })();
}

function devStateChangeCallback(e) {
    var toggle = this
    var tostate = $(this).prop("checked") ? "enabled" : "disabled";
    var devid = $(toggle).data("dev-id");
    var url = window.location.protocol + "//" + window.location.host +
	"/api/devices/" + devid + "/status/";

    if ($(this).data("in-progress")) {
	return;
    }
    
    $(this).data("in-progress", true);

    $.ajax({
	type: "POST",
	url: url,
	cache: false,
	dataType: "json",
	contentType: "application/json",
	data: JSON.stringify({
	    state: tostate
	}),

	timeout: 2000,

	success: function(data) {
	},

	error: function(jqXHR) {
	    displayWarning("Failed to change device state:" + jqXHR.statusText);
	    $(toggle).bootstrapToggle('toggle')
	},

	complete: function(data) {
	    $(toggle).removeData("in-progress");
	}
    });
}

$(function() {
    $(document).off("click.bs.toggle");
    $(document).on("click.bs.toggle", "div[data-toggle^=toggle]", function(e) {
	e.preventDefault();
	e.stopPropagation();
	var checkbox = $(this).find("input[type=checkbox]");
	if ($(checkbox).data("in-progress")) {
	    return false;
	}
	checkbox.bootstrapToggle('toggle');
    });
    
    var i;
    for (i = 0; i < devIds.length; i++) {
	var devid = "#dev-" + devIds[i] + "-toggle";
	$(devid).change(devStateChangeCallback);
    }

    monitorDevices();
})
