import os

import flask
from flask import (Flask,
                   Blueprint,
                   Response,
                   render_template,
                   send_file,
                   abort
)

from ihc.configurator.commander_client import CommanderClient
from ihc.configurator.configurator import Configurator

import ihc.configurator.forms

from ihc.configurator import (
    get_ihc_config,
    get_ihc_client,
)

module = Blueprint("webgui", __name__,
                   template_folder="templates",
                   static_folder="static",
                   static_url_path="/static/webgui")

@module.route("/", methods=["GET"])
def index():
    config = get_ihc_config()
    client = get_ihc_client()
    configurator = Configurator(client, config)
    states = configurator.get_all_states()
        
    return render_template("index.html", config=config,
                           devices=config.get_devices())

@module.route("/group/<groupid>", methods=["GET"])
def group(groupid):
    config = get_ihc_config()

    if not config.get_group_by_id(groupid):
        flask.abort(404)
    
    client = get_ihc_client()
    configurator = Configurator(client, config)
    states = configurator.get_all_states()
    
    return render_template("index.html", config=config,
                           devices=config.get_devices_in_group(groupid))
                       

@module.route("/config/", methods=["GET", "POST", "DELETE"])
def configure():
    config = get_ihc_config()
    return render_template("configure.html",
                           groups=config.get_groups(),
                           devices=config.get_devices())

@module.route("/config/export", methods=["GET"])
def export():
    config = get_ihc_config()
    resp = flask.make_response(flask.jsonify(**config.export()))
    resp.headers["Content-Type"] = "application/json"
    resp.headers["Content-Disposition"] = 'attachment; filename="ihc_config.json"'
    return resp

@module.route("/config/device/<devid>/edit", methods=["GET", "POST"])
def configure_device(devid):
    config = get_ihc_config()
    devconf = config.get_device_by_id(devid)
    if devconf is None:
        abort(404)
    
    return render_template("devconfig_edit.html", devconf=devconf)

@module.route("/config/group/<groupid>/edit/", methods=["GET", "POST"])
def group_edit(groupid):
    config = get_ihc_config()
    group = config.get_group_by_id(groupid)
    if group is None:
        abort(404)
        
    return render_template("group_edit.html")

@module.route("/config/device/add")
def add_device():
    return render_template("devconfig_add.html")

@module.route("/config/group/add")
def add_group():
    return render_template("group_add.html")
    
@module.route("/api/help/")
def api_help():
    import urllib

    app = flask.current_app
    
    routes={}
    for rule in app.url_map.iter_rules():
        if rule.endpoint == "static":
            continue

        if not rule.endpoint.startswith("api"):
            continue
        
        args = {}
        for arg in rule.arguments:
            args[arg] = "<%s>" % arg

        methods = filter(lambda m: m != "OPTIONS" and m != "HEAD", rule.methods)
        methods = ", ".join(methods)

        url = urllib.unquote(flask.url_for(rule.endpoint, **args))

        routes[rule.endpoint] = {
            "id": rule.endpoint,
            "url": url,
            "methods": methods,
        }
            
    return render_template("api_help.html",
                           routes=routes,
                           forms=ihc.configurator.forms,
    )
