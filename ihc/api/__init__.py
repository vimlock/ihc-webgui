import flask
import json

from flask import (
    Blueprint,
    abort,
    request,
    make_response,
    jsonify,
)

import ihc.configurator
from ihc.configurator import (
    Configurator,
    ConfigError,
    get_ihc_config,
    get_ihc_client,
    config,
)

module = Blueprint("api", __name__)

@module.route("/config/", methods=["GET", "POST"])
def api_config():
    if request.method == "GET":
        c = get_ihc_config()
        return jsonify(**c.export())
    elif request.method == "POST":
        config_file = request.files["config"]
        if not config_file:
            return make_response(jsonify(message="No file given"), 400)

        try:
            data = json.load(config_file.stream)
        except Exception as err:
            return make_response(jsonify(message=err.message), 400)
        
        try:
            c = config.parse(data)
        except ConfigError as err:
            return make_response(jsonify(message="Invalid file: " + err.message), 400)
        c.save("device_config.json")
        return jsonify()
    else:
        abort(405)

@module.route("/config/groups/", methods=["GET", "POST"])
def api_groups():
    config = get_ihc_config()
    if request.method == "GET":
        return jsonify(groups=config.get_group_ids())
    elif request.method == "POST":
        data = request.get_json()
        if data is None:
            data = {}
        form = ihc.configurator.forms.DeviceForm(**data)
        form.validate()
        return flask.jsonify(errors=form.errors)
    else:
        abort(405)


@module.route("/config/groups/<groupid>/", methods=["GET", "PATCH"])
def api_group(groupid):
    config = get_ihc_config()
    group = config.get_group_by_id(groupid)
    if group is None:
        abort(404)

    if request.method == "GET":
        return jsonify(
            id=group["id"],
            name=group["id"],
            devices=[dev["id"] for dev in group["devices"]],
        )
    else:
        abort(405)
    

@module.route("/config/groups/<groupid>/add/", methods=["POST"])
def api_add_device(groupid):
    pass

@module.route("/devices/", methods=["GET"])
def api_devices():
    config = get_ihc_config()
    return jsonify(
        devices=config.get_device_ids()
    )

@module.route("/devices/<device_ids>/config/", methods=["GET", "PATCH"])
def api_device_config(device_ids):
    config = get_ihc_config()

    ids = [s.strip() for s in device_ids.split(",")]

    devconfs = []
    for devid in ids:
        devconf = config.get_device_by_id(devid)
        if devconf is None:
            abort(404)
        devconfs.append(devconf)
    return jsonify(devices=devconfs)

@module.route("/devices/<device_ids>/status/", methods=["GET", "POST"])
def api_device_status(device_ids):
    config = get_ihc_config()
    client = get_ihc_client()
    configurator = Configurator(client, config)

    ids = [s.strip() for s in device_ids.split(",")]

    # verify that all the devices exist first so that not partial request is served
    for devid in ids:
        if not config.get_device_by_id(devid):
            flask.abort(404)

    if flask.request.method == "GET":
        responses = []
        for devid in ids:
            try:
                state = configurator.get_device_state(devid)
                responses.append({
                    "id": devid,
                    "state": "enabled" if state != 0 else "disabled"
                })
            except ConfigError as err:
                return flask.Response(str(err), status=400)
            
        return flask.jsonify(
            statuses=responses
        )

    else:
        if len(ids) != 1:
            abort(400)

        data = flask.request.get_json(force=True)
        if not "state" in data:
            abort(400)
        
        if data["state"] != "enabled" and data["state"] != "disabled":
            abort(400)

        devid = ids[0]
        tostate = True if data["state"] == "enabled" else False
            
        configurator.set_device_state(devid, tostate)
        return flask.jsonify()
        
        
