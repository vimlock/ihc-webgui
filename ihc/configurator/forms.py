import wtforms
from wtforms import (
    Form,
    TextField,
    IntegerField,
    SelectField,
    validators
)

from wtforms.validators import (
    Length,
    Required,
    NumberRange,
    ValidationError,
)

# (id, readable_name)
CONTROL_TYPES = [
    ("toggle", "Toggle"),
]

# (id, readable_name)
DEVICE_TYPES = [
    ("light", "Light"),
    ("heating", "Heating"),
    ("socket", "Socket"),
    ("other", "Other"),
]

class DeviceForm(Form):
    id = TextField("id", [Required(), Length(min=1, max=128)])
    name = TextField("name", [Required(), Length(min=1, max=128)])
    
    comment = TextField("comment")
    
    module = IntegerField("module", [Required(), NumberRange(min=0, max=8)])
    channel = IntegerField("channel", [Required(), NumberRange(min=0, max=8)])
    
    device_type = SelectField("device-type", [Required()], choices=DEVICE_TYPES)
    control_type = SelectField("control-type", [Required()], choices=CONTROL_TYPES)

    def validate_id(self, field):
        if not _id_regex.match(field.data):
            raise ValidationError("invalid device identifier")
 
class GroupForm(Form):
    id = TextField("id", [Required(), Length(min=1, max=128)])
    name = TextField("name", [Required(), Length(min=1, max=128)])
    comment = TextField("comment", [])

    def validate_id(self, field):
        if not _id_regex.match(ifeld.data):
            raise ValidationError("invalid group identifier")

        config = get_ihc_config()
        if config.get_group_by_id():
            raise ValidationError("group identifier already in use")


class StatusForm(Form):
    state = SelectField("state", [Required()], choices=[
        ("enabled", "Enabled"),
        ("disabled", "Disabled"),
    ])
