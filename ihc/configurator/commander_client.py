import socket
import flask

COMMANDER_PORT = 5001

CMD_ON      = 0 # set relay on
CMD_OFF     = 1 # set a relay off
CMD_DIMMER1 = 2 # ???
CMD_DIMMER2 = 3 # ???
CMD_DIMMER3 = 4 # ???
CMD_STATE   = 5 # get state of a relay
CMD_RESET   = 6 # reset all relays (presumably sets them off?)
CMD_STATUS  = 7 # get information about the micro controller

RES_SUCCESS = 1
RES_FAILURE = 0

ERR_UNKNOWN     = 0
ERR_BAD_COMMAND = 1
ERR_BAD_MODULE  = 2
ERR_BAD_CHANNEL = 3

def get_ihc_client():
    client = getattr(flask.g, "_ihc_commander_client", None)
    if client is not None:
        return client

    client = CommanderClient()
    flask.g._ihc_commander = client
    return client

def make_bytes(*args):
    return "".join(chr(n) for n in args)

def pack_nibble(high, low):
    if 0 > high > 0xF or 0 > low > 0xF:
        raise ValueError("invalid nibble size")
        
    return high << 4 | low

def unpack_nibble(n):
    return (n >> 4, n & 0xF)

def encode_cmd(n0, n1, n2, n3):
    return make_bytes(pack_nibble(n0, n1),
                      pack_nibble(n2, n3))

def decode_cmd(cmdstr):
    if len(cmdstr) != 2:
        raise ValueError("invalid command length %d" % len(cmdstr))

    (n0, n1) = unpack_nibble(ord(cmdstr[0]))
    (n2, n3) = unpack_nibble(ord(cmdstr[1]))
    return (n0, n1, n2, n3)

def errcode_str(errcode):
    return {
        ERR_BAD_COMMAND: "bad command",
        ERR_BAD_MODULE: "bad module",
        ERR_BAD_CHANNEL: "bad channel",
    }.get(errcode, "unknown error")

class CommanderError(Exception):
    def __init__(self, errcode):
        super(Exception, self).__init__(errcode_str(errcode))
        self.errcode = errcode

class CommanderClient(object):
    def __init__(self):
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.connect(('0.0.0.0', COMMANDER_PORT))

    def _sendcmd(self, cmd):
        self._socket.sendall(cmd)
        response = self._socket.recv(2)
        
        (cmd, success, errcode, data) = decode_cmd(response)
        if success != RES_SUCCESS:
            raise CommanderError(errcode)

        return data

    def get_channel_state(self, module, channel):
        cmd = encode_cmd(CMD_STATE, 0, module, channel)
        response = self._sendcmd(cmd)
        state =  response >> channel & 0x1

        return state
                          
    def set_channel_state(self, module, channel, value):
        cmdarg = CMD_ON if value else CMD_OFF
        cmd = encode_cmd(cmdarg, 0, module, channel)
        self._sendcmd(cmd)
