import re
import os
import json
import flask

import forms

__all__ = [
    "Config",
    "ConfigError",
    "load",
    "parse",
    "get_ihc_config",
]

class ConfigError(Exception):
    pass

def get_ihc_config():
    conf = getattr(flask.g, "_ihc_device_config", None)
    if conf is not None:
        return conf
    
    if os.path.isfile("device_config.json"):
        conf = load("device_config.json")
    elif os.path.isfile("device_config.local.json"):
        conf = load("device_config.local.json")
    else:
        conf = get_default()

    # Cache the config
    flask.g._ihc_device_config = conf
    
    return conf

# Used to provide more JSON-esque type names if an error occurs
_json_type_name = {
    dict: "object",
    list: "array",
    unicode: "string",
    str: "string",
    long: "number",
    int: "number",
    float: "number",
    bool: "bool",
    type(None): "null"
}

def required_property(target, name, proptype):
    if not name in target:
        raise ConfigError("property %s not defined" % name)

    val = target[name]
    if type(val) is not proptype:
        raise ConfigError("incorrect type for property %s, expecting %s, %s given" %
                         (name, _json_type_name[proptype], _json_type_name[type(val)]))
    
    return val

def optional_property(target, name, proptype, default):
    if not name in target:
        return default

    return required_property(target, name, proptype)

def validate_string(name, max_len, string):
    if len(string) == 0:
        raise ConfigError("%s can't be empty" % name)

    if len(string) > max_len:
        raise ConfigError("too long %s (%d max)" % (name, max_len))

_id_regex = re.compile('^[A-Za-z0-9_-]+$')

def valid_id(idstr):
    if not _id_match_regex.match(idstr):
        raise ConfigError("id contains invalid characters")

class Config(object):
    def __init__(self):
        self._devices = {}
        self._groups = {}

    def export(self):
        return {
            "version": 1,
            "groups": self._groups.values()
        }

    def save(self, filename):
        with open(filename, "wb") as f:
            json.dump(self.export(), f)

    def add_device(self, groupid, devobj):
        if type(devobj) != dict:
            raise ConfigError("configuration for a device must be a JSON object, %s given" %
                              _json_type_name[type(devobj)])

        required_property(devobj, "id", unicode)
        required_property(devobj, "device-type", unicode)

        required_property(devobj, "name", unicode)
        optional_property(devobj, "comment", unicode, "")

        required_property(devobj, "module", int)
        required_property(devobj, "channel", int)

        required_property(devobj, "control-type", unicode)

        id = devobj["id"]
        name = devobj["name"]
        module = devobj["module"]
        channel = devobj["channel"]
        ctrl_type = devobj["control-type"]
                
        validate_string("device id", 128, id)
        if self.get_device_by_id(id):
            raise ConfigError("device id '%s' is already in use" % id)
                          
        validate_string("device name", 128, name)
                          
        if module < 0 or module >= 16:
            raise ConfigError("invalid module id '%s'" % module)

        if channel < 0 or channel >= 8:
            raise ConfigError("invalid channel id '%s'" % channel)

        for devid, devconf in self._devices.iteritems():
            if devconf["module"] == module and devconf["channel"] == channel:
                raise ConfigError("channel %d in module %d is already in use" % (channel, module))
                
        if ctrl_type not in ["toggle", "dimmer"]:
            raise ConfigError("invalid control-type '%s'" % ctrl_type)
    
        # avoid errors with mutable references to the config
        devconf = devobj.copy()

        # assign the default values of optional keys to the config
        # they might not exist in the devobj
        devconf["comment"] = devobj.get("comment", "")

        self._groups[groupid]["devices"].append(devconf)
        self._devices[id] = devconf
        
    def remove_device(self, devid):
        if not devid in self._devices:
            return

        devobj = self._devices[devid]

        # remove the device from the group it is in
        for group in self._groups.values():
            if devobj in group["devices"]:
                group["devices"].remove(devobj)
                break

        del self._devices[devid]
            
    def get_device_ids(self):
        return self._devices.keys()
        
    def get_devices(self):
        return self._devices.values()

    def get_device_by_id(self, devid):
        return self._devices.get(devid, None)

    def add_group(self, groupid, groupname):
        if groupid in self._groups:
            raise ConfigError("duplicate group id '%s'" % groupid)

        self._groups[groupid] = {
            "id": groupid,
            "name": groupname,
            "devices": []
        }

    def remove_group(self, groupid):
        if groupid not in self._groups:
            return

        toremove = self._groups[groupid]["devices"]
        
        # remove any device in the group
        self._devices = {k: v for k, v in self._devices.items() if not v in toremove}
        del self._groups[groupid]

    def get_devices_in_group(self, groupid):
        return self._groups[groupid]["devices"]

    def get_group_by_id(self, groupid):
        return self._groups.get(groupid, None)

    def get_group_ids(self):
        return self._groups.keys()
        
    def get_groups(self):
        return self._groups.values()

def get_default():
    return Config()

def load(filename):
    try:
        fd = open(filename, "r")
    except Exception as e:
        raise ConfigError("failed to open file %s for reading: %s" %
                          (filename, e))
    try:
        jsonobj = json.load(open(filename))
    except Exception as e:
        raise ConfigError("invalid JSON: %s" % e)

    fd.close()

    return parse(jsonobj)
    
def parse(jsonobj):
    if type(jsonobj) is not dict:
        raise ConfigError("config must be a JSON object %s given" % _json_type_name[type(jsonobj)])
    
    version = required_property(jsonobj, "version", int)

    if version != 1:
        raise ConfigError("unsupported config version: %s" % version)

    conf = Config()
    groupobjs = required_property(jsonobj, "groups", list)
    for gnum, groupobj in enumerate(groupobjs):
        try:
            parse_group(conf, groupobj)
        except ConfigError as err:
            raise ConfigError("config.groups[%d] %s" % (gnum, err.message))
        
    return conf

def parse_group(conf, groupobj):
    if type(groupobj) is not dict:
        raise ConfigError("group must be JSON objects")

    groupid = required_property(groupobj, "id", unicode)
    groupname = required_property(groupobj, "name", unicode)

    conf.add_group(groupid, groupname)

    devobjs = required_property(groupobj, "devices", list)
    for dnum, devobj in enumerate(devobjs):
        try:
            conf.add_device(groupid, devobj)
        except ConfigError as err:
            raise ConfigError("device[%d]: %s" % (dnum, err.message))
