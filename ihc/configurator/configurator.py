class Configurator(object):
    def __init__(self, client, config):
        self._client = client
        self._config = config

    def get_all_states(self):
        res = {}
        for devconf in self._config.get_devices():
            res[devconf["id"]] = self.get_device_state(devconf["id"])

        return res

    def get_device_state(self, devid):
        devconf = self._config.get_device_by_id(devid)
        if devconf is None:
            raise ConfigError("invalid device id")

        module = devconf["module"]
        channel = devconf["channel"]
                          
        return self._client.get_channel_state(module, channel)
                          
    def set_device_state(self, devid, state):
        devconf = self._config.get_device_by_id(devid)
        if devconf is None:
            raise ConfigError("invalid device id")

        module = devconf["module"]
        channel = devconf["channel"]
        
        self._client.set_channel_state(module, channel, state)

            
