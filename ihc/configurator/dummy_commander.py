import socket
import threading
import select

import commander_client
from commander_client import encode_cmd, decode_cmd

from commander_client import (
    RES_SUCCESS, RES_FAILURE,
    CMD_ON, CMD_OFF,
    CMD_DIMMER1, CMD_DIMMER2, CMD_DIMMER3,
    CMD_STATE, CMD_RESET, CMD_STATUS,
    ERR_UNKNOWN,
    ERR_BAD_COMMAND,
    ERR_BAD_MODULE,
    ERR_BAD_CHANNEL,)

class DummyChannel(object):
    def __init__(self, id):
        self.id = id
        self.state = 0
        self.dimming = 0

class DummyModule(object):
    def __init__(self, id):
        self._channels = [DummyChannel(n) for n in range(8)]

class DummyCommander(object):
    def __init__(self):
        self._modules = [DummyModule(n) for n in range(16)]

    def handle_cmd(self, cmdbytes):
        (cmd, chk, mod, chn) = decode_cmd(cmdbytes)

        if cmd > 7:
            return encode_cmd(cmd, RES_FAILURE, ERR_BAD_COMMAND, 0)

        if chn > 7:
            return encode_cmd(cmd, RES_FAILURE, ERR_BAD_CHANNEL, 0)

        if cmd == CMD_ON:
            # print("CMD_ON %d.%d" % (mod, chn))
            self.set_channel_state(mod, chn, 1)
            return encode_cmd(cmd, RES_SUCCESS, 0, 0)

        elif cmd == CMD_OFF:
            # print("CMD_OFF %d.%d" % (mod, chn))
            self.set_channel_state(mod, chn, 0)
            return encode_cmd(cmd, RES_SUCCESS, 0, 0)

        elif cmd == CMD_DIMMER1 or cmd == CMD_DIMMER2 or cmd == CMD_DIMMER3:
            self.set_channel_dimming(mod, chn, 1 + cmd - CMD_DIMMER1)
            return encode_cmd(cmd, RES_SUCCESS, 0, 0)
        
        elif cmd == CMD_STATE:
            # the channel argument is ignored here, methinks

            channels = self._modules[mod]._channels
            response = 0
            for n, c in enumerate(channels):
                if c.state == 0:
                    continue
                response |= 1 << n
            
            return encode_cmd(cmd, RES_SUCCESS, 0, response)
        
        elif cmd == CMD_RESET:
            # wtf does this command do? is it just shorthand to reset all?
            for n in range(8):
                self.set_channel_state(mod, n, 0)
                
            return encode_cmd(cmd, RES_SUCCESS, 0, 0)
            
        elif cmd == CMD_STATUS:
            return encode_cmd(cmd, RES_SUCCESS, 1, 0)
            
        

    def get_channel_state(self, module, channel):
        return self._modules[module]._channels[channel].state

    def set_channel_state(self, module, channel, state):
        self._modules[module]._channels[channel].state = 1 if state else 0

    def get_channel_dimming(self, module, channel):
        return self._modules[module]._channels[channel].dimming
    
    def set_channel_dimming(self, module, channel, dimming):
        if dimming not in [1, 2, 3]:
            raise ValueError("bad dimming value %s" % dimming)

        self._modules[module]._channels[channel].dimming = dimming


class Client(threading.Thread):
    def __init__(self, addr, commander, lock, socket):
        super(Client, self).__init__()
        self._addr = addr
        self._socket = socket
        self._commander = commander
        self._commander_lock = lock

    def run(self):
        while True:
            cmd = self._socket.recv(2)
            if len(cmd) != 2:
                break

            # print("received command %x %x" % (ord(cmd[0]), ord(cmd[1])))

            with self._commander_lock:
                response = cmdr.handle_cmd(cmd)
                # print("sending response %x %x" % (ord(response[0]), ord(response[1])))
                self._socket.sendall(response)
                
        self._socket.close()
        print("connection from %s closed" % str(addr))

        
if __name__ == "__main__":
    cmdr = DummyCommander()
    cmdr_lock = threading.Lock()

    try:
        sfd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sfd.bind(('0.0.0.0', 5001))
        sfd.listen(1)
    except:
        print("failed to open socket")
        raise

    print("dummy IHC-commander listening on port %d" % 5001)

    clients = []
    
    try:
        while True:
            (rlist, wlist, xlist) = select.select([sfd], [], [], 0.1)
            if sfd in rlist:
                conn, addr = sfd.accept()
                print("accepted connection from %s" % str(addr))

                clt = Client(addr, cmdr, cmdr_lock, conn)
                clt.start()
                clients.append(clt)

            for c in clients:
                if not c.isAlive():
                    c.join()
            
            # collect dead children
            clients = [c for c in clients if c.isAlive()]
                
    finally:
        for clt in clients:
            clt.join()

        sfd.close()
