import unittest
import ihc
from ihc.commander.controller import parse_config
from flask import json

class ConfigLoadTest(unittest.TestCase):
    def runTest(self):
        jsonobj = json.load(file("test/test_files/valid_conf.json"))
        parse_config(jsonobj)
    
class InvalidConfigLoadTest(unittest.TestCase):
    def runTest(self):
        jsonobj = json.load(file("test/test_files/invalid_conf.json"))
        with self.assertRaises(ihc.commander.controller.ConfigError):
            parse_config(jsonobj)

class LoadStoreConfigTest(unittest.TestCase):
    def runTest(self):
        jsonobj = json.load(file("test/test_files/valid_conf.json"))
        
                          
                          
                          
